import 'package:flutter/material.dart';
import 'package:test_app/app.dart';
import 'package:test_app/core/app_global_state.dart';

Future<void> main() async {
  await AppGlobalState().appSetUp();
  runApp(const App());
}
