import 'package:flutter/material.dart';
import 'package:test_app/core/components/style.dart';
import 'package:test_app/core/components/ui_helpers.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(35),
                color: kBlue,
              ),
            ),
            horizontalSpace10,
            const Text(
              'Test Logo',
              style: kBoldWhite24,
            ),
          ],
        ),
      ),
    );
  }
}
