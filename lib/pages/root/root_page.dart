import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/pages/auth/presentation/login_page.dart';
import 'package:test_app/pages/home/cubit/partners_cubit.dart';
import 'package:test_app/pages/home/data/repository/partners_repository.dart';
import 'package:test_app/pages/home/presentation/home_page.dart';
import 'package:test_app/pages/root/widgets/splash_screen.dart';

class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  late PartnersCubit _partnersCubit;

  @override
  void initState() {
    super.initState();
    _partnersCubit = PartnersCubit(PartnersRepository());
    _partnersCubit.getPartners();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<PartnersCubit, PartnersState>(
        bloc: _partnersCubit,
        listener: (context, state) {
          if (state is PartnersLoaded) {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const HomePage()));
          } else if (state is PartnersError) {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const LoginPage()));
          }
        },
        builder: (context, state) {
          return const SplashScreen();
        },
      ),
    );
  }
}
