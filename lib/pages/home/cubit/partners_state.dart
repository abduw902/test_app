part of 'partners_cubit.dart';

abstract class PartnersState extends Equatable {
  const PartnersState();
}

class PartnersInitial extends PartnersState {
  @override
  List<Object> get props => [];
}

class PartnersLoading extends PartnersState {
  @override
  List<Object?> get props => [];
}

class PartnersLoaded extends PartnersState {
  final PartnersResponseModel partnersResponseModel;

  const PartnersLoaded(this.partnersResponseModel);

  @override
  List<Object?> get props => [partnersResponseModel];
}

class PartnersError extends PartnersState {
  final String message;

  const PartnersError(this.message);

  @override
  List<Object?> get props => [message];
}
