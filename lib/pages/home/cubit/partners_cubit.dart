import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/core/constants/error_messages.dart';
import 'package:test_app/core/error/exception.dart';
import 'package:test_app/pages/home/data/models/partners_response_model.dart';
import 'package:test_app/pages/home/data/repository/partners_repository.dart';

part 'partners_state.dart';

class PartnersCubit extends Cubit<PartnersState> {
  final PartnersRepository _partnersRepository;

  PartnersCubit(this._partnersRepository) : super(PartnersInitial());

  Future<void> getPartners() async {
    const int pageNumber = 0;
    const int pageSize = 10;
    try {
      emit(PartnersLoading());
      final result =
          await _partnersRepository.getPartners(pageNumber, pageSize);
      emit(PartnersLoaded(result));
    } on ServerException {
      emit(const PartnersError(serverErrorMsg));
    } on SocketException {
      emit(const PartnersError(noInternetErrorMsg));
    } catch (e) {
      emit(PartnersError(e.toString()));
    }
  }
}
