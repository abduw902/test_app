class PartnerModel {
  PartnerModel({
    this.id,
    this.title,
    this.desc,
    this.logo,
  });

  int? id;
  String? title;
  String? desc;
  String? logo;

  factory PartnerModel.fromJson(Map<String, dynamic> json) => PartnerModel(
        id: json["id"],
        title: json["title"],
        desc: json["desc"],
        logo: json["logo"],
      );
}
