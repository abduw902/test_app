import 'package:test_app/core/data/models/response_model.dart';
import 'package:test_app/pages/home/data/models/partner_model.dart';

class PartnersResponseModel {
  ResponseModel? responseModel;

  PartnersResponseModel({this.responseModel});

  factory PartnersResponseModel.fromJson(Map<String, dynamic> json) =>
      PartnersResponseModel(
        responseModel: ResponseModel(
            status: json["status"],
            message: json["message"],
            data: Data.fromJson(json["data"])),
      );
}

class Data {
  Data({
    this.partners,
    this.pagesCount,
    this.rowsCount,
  });

  List<PartnerModel>? partners;
  int? pagesCount;
  int? rowsCount;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        partners: List<PartnerModel>.from(
            json["data"].map((x) => PartnerModel.fromJson(x))),
        pagesCount: json["pagesCount"],
        rowsCount: json["rowsCount"],
      );
}
