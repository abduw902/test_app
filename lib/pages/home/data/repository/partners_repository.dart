import 'package:test_app/core/constants/api.dart';
import 'package:test_app/core/utils/api_client.dart';
import 'package:test_app/pages/home/data/models/partners_response_model.dart';

class PartnersRepository {
  Future<PartnersResponseModel> getPartners(
      int pageNumber, int pageSize) async {
    final response = await ApiClient.getRequest(
        "${API.URL}/partners?pageNumber=$pageNumber&pageSize=$pageSize");
    return PartnersResponseModel.fromJson(response);
  }
}
