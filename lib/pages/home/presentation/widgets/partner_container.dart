import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:test_app/core/components/shimmer_widget.dart';
import 'package:test_app/core/components/style.dart';
import 'package:test_app/core/components/ui_helpers.dart';

class PartnerContainer extends StatelessWidget {
  final String imgUrl;
  final String title;
  final String description;

  const PartnerContainer(
      {Key? key,
      required this.imgUrl,
      required this.title,
      required this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: CachedNetworkImage(
              height: 72.0,
              width: 72.0,
              imageUrl: imgUrl,
              fit: BoxFit.cover,
              placeholder: (context, _) => ShimmerWidget.rectangular(
                  height: 72.0,
                  shapeBorder: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0))),
              errorWidget: (ctx, _, __) {
                return ShimmerWidget.rectangular(
                    height: 72.0,
                    shapeBorder: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0)));
              },
            ),
          ),
          horizontalSpace8,
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: kBoldWhite16,
                ),
                verticalSpace8,
                Text(
                  description,
                  style: kRegularGrey16,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
