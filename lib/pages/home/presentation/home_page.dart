import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/core/components/custom_appbar.dart';
import 'package:test_app/core/components/loading_widget.dart';
import 'package:test_app/core/components/style.dart';
import 'package:test_app/core/components/ui_helpers.dart';
import 'package:test_app/pages/home/cubit/partners_cubit.dart';
import 'package:test_app/pages/home/data/models/partner_model.dart';
import 'package:test_app/pages/home/data/repository/partners_repository.dart';
import 'package:test_app/pages/home/presentation/widgets/partner_container.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<PartnerModel>? partners;
  late PartnersCubit _partnersCubit;

  @override
  void initState() {
    super.initState();
    _partnersCubit = PartnersCubit(PartnersRepository());
    _partnersCubit.getPartners();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: CustomAppBar(
        implyBackButton: false,
        title: "Partners",
      ),
      body: BlocBuilder<PartnersCubit, PartnersState>(
        bloc: _partnersCubit,
        builder: (context, state) {
          if (state is PartnersLoaded) {
            partners = state.partnersResponseModel.responseModel!.data.partners
                as List<PartnerModel>;
            return ListView.separated(
                itemBuilder: (context, index) => PartnerContainer(
                    imgUrl: partners![index].logo!,
                    title: partners![index].title!,
                    description: partners![index].desc!),
                separatorBuilder: (context, index) => verticalSpace10,
                itemCount: partners!.length);
          } else {
            return const BuildLoading();
          }
        },
      ),
    );
  }
}
