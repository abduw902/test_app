import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:otp_autofill/otp_autofill.dart';
import 'package:test_app/core/app_global_state.dart';
import 'package:test_app/core/components/custom_appbar.dart';
import 'package:test_app/core/components/custom_main_button.dart';
import 'package:test_app/core/components/style.dart';
import 'package:test_app/core/components/ui_helpers.dart';
import 'package:test_app/core/constants/hive_const_keys.dart';
import 'package:test_app/core/utils/toast_utils.dart';
import 'package:test_app/pages/auth/cubit/auth_cubit.dart';
import 'package:test_app/pages/auth/presentation/code_verification/widgets/pin_box_number.dart';
import 'package:test_app/pages/home/presentation/home_page.dart';

class CodeVerificationPage extends StatefulWidget {
  final String requestId;
  final String phoneNumber;

  const CodeVerificationPage({
    Key? key,
    required this.requestId,
    required this.phoneNumber,
  }) : super(key: key);

  @override
  _CodeVerificationPageState createState() => _CodeVerificationPageState();
}

class _CodeVerificationPageState extends State<CodeVerificationPage> {
  late String requestId;
  bool isLoading = false;
  int pinFieldsAmount = 4;
  late List<Widget> pinBoxList;
  late OTPTextEditController controller;
  late OTPInteractor _otpInteractor;
  final pinTextEditingController = TextEditingController();
  ValueNotifier<String> pinText = ValueNotifier("");

  @override
  void initState() {
    super.initState();
    _otpInteractor = OTPInteractor();
    _otpInteractor.getAppSignature();

    controller = OTPTextEditController(
      codeLength: 4,
      onCodeReceive: (code) {
        if (code.length == pinFieldsAmount) {
          setState(() {
            pinText.value = code;
            pinTextEditingController.text = code;
          });
        }
      },
      otpInteractor: _otpInteractor,
    )..startListenUserConsent(
        (code) {
          final exp = RegExp(r'(\d{4})');
          return exp.stringMatch(code ?? '') ?? '';
        },
      );

    requestId = widget.requestId;
  }

  @override
  void dispose() {
    pinTextEditingController.dispose();
    _otpInteractor.stopListenForCode();
    controller.dispose();
    pinText.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    pinBoxList = List.generate(
      pinFieldsAmount,
      (index) => pinText.value.length > index
          ? Text(
              pinText.value.substring(
                index,
                index + 1,
              ),
              style: kBoldWhite24,
            )
          : const Text(""),
    );
    return BlocConsumer<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is CodeSending || state is Authorizing) {
          isLoading = true;
        } else if (state is CodeSendingError) {
          isLoading = false;
          ToastUtils.showShortToast(context, state.message);
        } else if (state is AuthorizationError) {
          isLoading = false;
          ToastUtils.showShortToast(context, state.message);
        } else if (state is CodeSent) {
          isLoading = false;
          requestId = state.requestId;
        } else if (state is AuthorizationSuccess) {
          AppGlobalState.hive.put(
            HiveKeys.authorizationToken,
            state.authResponseModel.responseModel!.data.accessToken.toString(),
          );
          isLoading = false;
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => const HomePage()));
        }
      },
      builder: (context, state) {
        return LoadingOverlay(
          isLoading: isLoading,
          child: KeyboardDismisser(
            child: Scaffold(
              backgroundColor: kBackgroundColor,
              appBar: CustomAppBar(),
              body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Verification Code',
                      style: kBoldWhite24,
                    ),
                    verticalSpace8,
                    const Text(
                      'Enter the verification code that we have sent to your phone',
                      style: kRegularGrey16,
                    ),
                    verticalSpace25,
                    Stack(
                      children: [
                        ValueListenableBuilder(
                          valueListenable: pinText,
                          builder: (context, pinTextValue, _) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: List.generate(
                                pinFieldsAmount,
                                (index) => PinBoxNumber(pinBoxList[index]),
                              ),
                            );
                          },
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Column(
                            children: [
                              TextField(
                                controller: pinTextEditingController,
                                autofocus: true,
                                showCursor: false,
                                enableInteractiveSelection: false,
                                keyboardType: TextInputType.number,
                                style: const TextStyle(
                                  color: Colors.transparent,
                                ),
                                decoration: const InputDecoration(
                                  border: InputBorder.none,
                                ),
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly,
                                  LengthLimitingTextInputFormatter(
                                    pinFieldsAmount,
                                  ),
                                ],
                                onChanged: (value) async {
                                  pinText.value = pinTextEditingController.text;
                                  pinBoxList = List.generate(
                                    pinFieldsAmount,
                                    (index) => pinText.value.length > index
                                        ? Text(
                                            pinText.value.substring(
                                              index,
                                              index + 1,
                                            ),
                                            style: kBoldWhite24,
                                          )
                                        : const Text(""),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    verticalSpace30,
                    Row(
                      children: [
                        const Text(
                          "Didn't receive the code? ",
                          style: kBoldWhite14,
                        ),
                        GestureDetector(
                            onTap: () {
                              BlocProvider.of<AuthCubit>(context)
                                  .sendSms(phoneNumber: widget.phoneNumber);
                            },
                            child: Text(
                              "Send again",
                              style: kBoldWhite14.copyWith(color: kBlue),
                            ))
                      ],
                    ),
                    const Spacer(),
                    CustomMainButton(
                        title: "Continue", onTap: () => verifyCode()),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void verifyCode() {
    if (pinTextEditingController.text.length == pinFieldsAmount) {
      BlocProvider.of<AuthCubit>(context).signIn(
        requestId: requestId,
        verificationCode: pinTextEditingController.text,
      );
    }
  }
}
