import 'package:flutter/material.dart';
import 'package:test_app/core/components/style.dart';

class PinBoxNumber extends StatelessWidget {
  final Widget? pinNumber;

  const PinBoxNumber(
    this.pinNumber, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: SizedBox(
        width: 56,
        height: 56,
        child: Container(
          decoration: const BoxDecoration(
            color: kLightBlack,
            borderRadius: BorderRadius.all(
              Radius.circular(12),
            ),
          ),
          child: Center(child: pinNumber),
        ),
      ),
    );
  }
}
