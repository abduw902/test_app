import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test_app/core/components/style.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController controller;
  final String inputMask;

  const CustomTextField({
    Key? key,
    required this.controller, required this.inputMask,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16), color: kBackgroundColor),
      child: TextFormField(
        cursorColor: kWhite,
        style: kRegularGrey16,
        controller: controller,
        textInputAction: TextInputAction.done,
        inputFormatters: [
          MaskedInputFormatter(inputMask),
        ],
        keyboardType: TextInputType.number,
        keyboardAppearance: Brightness.dark,
        decoration: InputDecoration(
            hintStyle: kRegularGrey16,
            enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: kGrey),
                borderRadius: BorderRadius.circular(16)),
            focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: kBlue),
                borderRadius: BorderRadius.circular(16)),
            prefixIcon: Padding(
              padding: const EdgeInsets.all(12.0),
              child: SvgPicture.asset("assets/svg_icons/call_icon.svg"),
            ),
            prefix: const Text('+998 ')),
      ),
    );
  }
}
