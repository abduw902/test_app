import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:test_app/core/components/custom_appbar.dart';
import 'package:test_app/core/components/custom_main_button.dart';
import 'package:test_app/core/components/style.dart';
import 'package:test_app/core/components/ui_helpers.dart';
import 'package:test_app/core/utils/toast_utils.dart';
import 'package:test_app/pages/auth/cubit/auth_cubit.dart';
import 'package:test_app/pages/auth/presentation/code_verification/code_verification_page.dart';
import 'package:test_app/pages/auth/presentation/widgets/custom_text_field.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late TextEditingController _phoneNumberController;
  final String inputMask = '00 000 00 00';
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    _phoneNumberController = TextEditingController();
  }

  @override
  void dispose() {
    _phoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      isLoading: isLoading,
      child: KeyboardDismisser(
        child: BlocConsumer<AuthCubit, AuthState>(
          listener: (context, state) {
            if (state is CodeSent) {
              isLoading = false;
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CodeVerificationPage(
                          requestId: state.requestId,
                          phoneNumber: _phoneNumberController.text)));
            }
            if (state is CodeSendingError) {
              isLoading = false;
              ToastUtils.showShortToast(context, state.message);
            }
            if (state is CodeSending) {
              isLoading = true;
            }
          },
          builder: (context, state) {
            return Scaffold(
              appBar: CustomAppBar(
                implyBackButton: false,
              ),
              backgroundColor: kBackgroundColor,
              body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Hey! Welcome back",
                      style: kBoldWhite24,
                    ),
                    verticalSpace8,
                    const Text(
                      "Sign in to your account",
                      style: kRegularGrey16,
                    ),
                    verticalSpace30,
                    CustomTextField(
                      inputMask: inputMask,
                      controller: _phoneNumberController,
                    ),
                    const Spacer(),
                    CustomMainButton(
                        title: "Login",
                        onTap: () {
                          if (_phoneNumberController.text.isNotEmpty &&
                              _phoneNumberController.text.length ==
                                  inputMask.length) {
                            BlocProvider.of<AuthCubit>(context).sendSms(
                                phoneNumber: _phoneNumberController.text);
                          } else {
                            ToastUtils.showShortToast(
                                context, 'Phone number is incorrect');
                          }
                        }),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
