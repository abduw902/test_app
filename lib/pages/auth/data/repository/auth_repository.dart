import 'package:test_app/core/constants/api.dart';
import 'package:test_app/core/utils/api_client.dart';
import 'package:test_app/pages/auth/data/models/auth_response_model.dart';
import 'package:test_app/pages/auth/data/models/verification_response_model.dart';

class AuthRepository {
  Future<AuthResponseModel> signIn(
      String requestId, String verificationCode) async {
    final response = await ApiClient.postRequest('${API.AUTH}/auth/signin',
        {"requestId": requestId, "password": verificationCode});
    return AuthResponseModel.fromJson(response);
  }

  Future<String> sendSms(String phoneNumber) async {
    final response = await ApiClient.postRequest(
        '${API.AUTH}/auth/signin/request',
        {"type": "Phone", "login": phoneNumber});
    return VerificationResponseModel.fromJson(response)
        .responseModel!
        .data
        .requestId
        .toString();
  }
}
