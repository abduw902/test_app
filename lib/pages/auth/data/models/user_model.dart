class UserModel {
  UserModel({
    this.id,
    this.lastName,
    this.firstName,
    this.middleName,
    this.userName,
    this.birthday,
    this.gender,
    this.phoneNumber,
    this.email,
    this.avatar,
    this.rating,
    this.roles,
  });

  int? id;
  String? lastName;
  String? firstName;
  String? middleName;
  String? userName;
  DateTime? birthday;
  String? gender;
  String? phoneNumber;
  String? email;
  String? avatar;
  int? rating;
  List<String>? roles;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"],
        lastName: json["lastName"],
        firstName: json["firstName"],
        middleName: json["middleName"],
        userName: json["userName"],
        birthday:
            json["birthday"] != null ? DateTime.parse(json["birthday"]) : null,
        gender: json["gender"],
        phoneNumber: json["phoneNumber"],
        email: json["email"],
        avatar: json["avatar"],
        rating: json["rating"],
        roles: json['roles'] != null
            ? json['roles'].cast<String>()
            : [],
      );
}
