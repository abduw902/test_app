import 'package:test_app/core/data/models/response_model.dart';
import 'package:test_app/pages/auth/data/models/user_model.dart';

class AuthResponseModel {
  ResponseModel? responseModel;

  AuthResponseModel({
    this.responseModel,
  });

  factory AuthResponseModel.fromJson(Map<String, dynamic> json) =>
      AuthResponseModel(
        responseModel: ResponseModel(
          status: json["status"],
          message: json["message"],
          data: Data.fromJson(json["data"]),
        ),
      );
}

class Data {
  Data({
    this.accessToken,
    this.refreshToken,
    this.issuedDate,
    this.accessExpiresDate,
    this.refreshExpiresDate,
    this.user,
  });

  String? accessToken;
  String? refreshToken;
  DateTime? issuedDate;
  DateTime? accessExpiresDate;
  DateTime? refreshExpiresDate;
  UserModel? user;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        accessToken: json["accessToken"],
        refreshToken: json["refreshToken"],
        issuedDate: DateTime.parse(json["issuedDate"]),
        accessExpiresDate: DateTime.parse(json["accessExpiresDate"]),
        refreshExpiresDate: DateTime.parse(json["refreshExpiresDate"]),
        user: UserModel.fromJson(json["user"]),
      );
}
