import 'package:test_app/core/data/models/response_model.dart';

class VerificationResponseModel {
  ResponseModel? responseModel;

  VerificationResponseModel({
    this.responseModel,
  });

  factory VerificationResponseModel.fromJson(Map<String, dynamic> json) =>
      VerificationResponseModel(
        responseModel: ResponseModel(
            status: json["status"],
            message: json["message"],
            data: Data.fromJson(json["data"])),
      );
}

class Data {
  String? requestId;

  Data({this.requestId});

  factory Data.fromJson(Map<String, dynamic> json) =>
      Data(requestId: json["requestId"]);
}
