import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/core/constants/error_messages.dart';
import 'package:test_app/core/error/exception.dart';
import 'package:test_app/pages/auth/data/models/auth_response_model.dart';
import 'package:test_app/pages/auth/data/repository/auth_repository.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepository _authRepository;

  AuthCubit(this._authRepository) : super(AuthInitial());

  Future<void> sendSms({required String phoneNumber}) async {
    try {
      emit(CodeSending());
      final result =
          await _authRepository.sendSms("998$phoneNumber".replaceAll(' ', ''));
      emit(CodeSent(result));
    } on ServerException {
      emit(const CodeSendingError(serverErrorMsg));
    } on SocketException {
      emit(const CodeSendingError(noInternetErrorMsg));
    } catch (e) {
      emit(CodeSendingError(e.toString()));
    }
  }

  Future<void> signIn(
      {required String requestId, required String verificationCode}) async {
    try {
      emit(CodeSending());
      final result = await _authRepository.signIn(requestId, verificationCode);
      emit(AuthorizationSuccess(result));
    } on ServerException {
      emit(const AuthorizationError(serverErrorMsg));
    } on SocketException {
      emit(const AuthorizationError(noInternetErrorMsg));
    } catch (e) {
      emit(AuthorizationError(e.toString()));
    }
  }
}
