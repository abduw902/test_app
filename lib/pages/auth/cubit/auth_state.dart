part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthInitial extends AuthState {
  @override
  List<Object> get props => [];
}

class CodeSending extends AuthState {
  @override
  List<Object?> get props => [];
}

class CodeSent extends AuthState {
  final String requestId;

  const CodeSent(this.requestId);

  @override
  List<Object?> get props => [requestId];
}

class CodeSendingError extends AuthState {
  final String message;

  const CodeSendingError(this.message);

  @override
  List<Object?> get props => [message];
}

class Authorizing extends AuthState {
  @override
  List<Object?> get props => [];
}

class AuthorizationSuccess extends AuthState {
  final AuthResponseModel authResponseModel;

  const AuthorizationSuccess(this.authResponseModel);
  @override
  List<Object?> get props => [authResponseModel];
}

class AuthorizationError extends AuthState {
  final String message;

  const AuthorizationError(this.message);

  @override
  List<Object?> get props => [message];
}
