import 'package:flutter/material.dart';
import "package:flutter_bloc/flutter_bloc.dart";
import 'package:test_app/pages/auth/cubit/auth_cubit.dart';
import 'package:test_app/pages/auth/data/repository/auth_repository.dart';

class DependenciesProvider extends StatelessWidget {
  final Widget child;

  const DependenciesProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthCubit(
            AuthRepository(),
          ),
        ),
      ],
      child: child,
    );
  }
}
