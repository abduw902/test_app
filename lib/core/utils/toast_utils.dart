import 'package:flutter/material.dart';
import 'package:test_app/core/components/style.dart';

// ignore: avoid_classes_with_only_static_members
class ToastUtils {
  /// show toast message
  static void showShortToast(BuildContext context, String message,
      [Color? color]) {
    dismissToast(context);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: color ?? Colors.black.withOpacity(0.9),
        duration: const Duration(seconds: 3),
        content: Row(
          children: [
            Flexible(
              child: Text(
                message,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: kBoldWhite16,
              ),
            ),
          ],
        ),
      ),
    );
  }

  static void dismissToast(BuildContext context) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
  }
}
