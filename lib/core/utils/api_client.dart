import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:test_app/core/app_global_state.dart';
import 'package:test_app/core/constants/hive_const_keys.dart';
import 'package:test_app/core/error/exception.dart';
import 'package:test_app/core/utils/http_utils.dart';
import 'package:test_app/core/utils/network_state.dart';

// ignore: avoid_classes_with_only_static_members
abstract class ApiClient {
  ///Get Request
  static Future<dynamic> getRequest(
    String url,
  ) async =>
      _request(HttpUtil.request(url, requestType: get));

  ///Post Request
  static Future<dynamic> postRequest(
          String url, Map<String, dynamic> body) async =>
      _request(HttpUtil.request(url, requestType: post, body: body));

  ///Put Request
  static Future<dynamic> putRequest(
          String url, Map<String, dynamic> body) async =>
      _request(HttpUtil.request(url, requestType: put, body: body));

  ///Patch Request
  static Future<dynamic> patchRequest(
          String url, Map<String, dynamic> body) async =>
      _request(HttpUtil.request(url, requestType: patch, body: body));

  ///Delete Request
  static Future<dynamic> deleteRequest(String url,
          {Map<String, dynamic>? body}) async =>
      _request(HttpUtil.request(url, requestType: delete, body: body ?? {}));

  ///MultiPart Request
  static Future<dynamic> multiPartUpload(
          String uploadingDataType, String url, String filePath) async =>
      _request(HttpUtil.uploadMultiPart(uploadingDataType, url, filePath),
          isMultiPartRequest: true);

  static Future<dynamic> _request(Future<Response> request,
      {bool isMultiPartRequest = false}) async {
    if (!await NetworkState.isInternetConnected()) {
      throw const SocketException("No internet connection");
    }
    final Response response = await request;

    if (response.statusCode == 200 || response.statusCode == 201) {
      if (isMultiPartRequest) {
        try {
          return json.decode(utf8.decode(response.bodyBytes));
        } on FormatException {
          return response.body;
        }
      }

      if (response.headers['authorization'] != null) {
        await AppGlobalState.hive.put(HiveKeys.authorizationToken,
            response.headers['authorization'].toString());
      }

      if (response.body.isNotEmpty) {
        return json.decode(utf8.decode(response.bodyBytes));
      }

      return;
    } else {
      throw ServerException(int.parse(response.statusCode.toString()));
    }
  }
}
