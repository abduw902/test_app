class ResponseModel {
  ResponseModel({
    this.status,
    this.message,
    this.errors,
    this.data,
  });

  String? status;
  String? message;
  dynamic errors;
  dynamic data;
}
