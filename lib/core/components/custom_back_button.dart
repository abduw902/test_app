import 'package:flutter/cupertino.dart';
import 'package:test_app/core/components/style.dart';

class CustomBackButton extends StatelessWidget {
  const CustomBackButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: kLightBlack,
          ),
          child: const FittedBox(
            child: Icon(
              CupertinoIcons.back,
              color: kWhite,
            ),
          ),
        ),
      ),
    );
  }
}
