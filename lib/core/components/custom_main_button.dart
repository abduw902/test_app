import 'package:flutter/material.dart';
import 'package:test_app/core/components/style.dart';

class CustomMainButton extends StatelessWidget {
  final String title;
  final VoidCallback onTap;

  const CustomMainButton({Key? key, required this.title, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 56,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: kBlue,
        ),
        child: Center(
          child: Text(
            title,
            style: kBoldWhite14,
          ),
        ),
      ),
    );
  }
}
