import 'package:flutter/material.dart';

const Color kBlack = Color.fromRGBO(51, 51, 51, 1);
const Color kGrey = Color.fromRGBO(160, 174, 192, 1);
const Color kBlue = Color.fromRGBO(69, 90, 247, 1);
const Color kWhite = Color.fromRGBO(255, 255, 255, 1);
const Color kBackgroundColor = Color.fromRGBO(26, 32, 44, 1);
const Color kLightBlack = Color.fromRGBO(39, 48, 63, 1);

const kRegularGrey16 =
    TextStyle(fontWeight: FontWeight.w400, fontSize: 16.0, color: kGrey);

const kBoldWhite24 =
    TextStyle(fontWeight: FontWeight.w700, fontSize: 24.0, color: kWhite);
const kBoldWhite16 =
    TextStyle(fontWeight: FontWeight.w700, fontSize: 16.0, color: kWhite);
const kBoldWhite14 =
    TextStyle(fontWeight: FontWeight.w700, fontSize: 14.0, color: kWhite);
