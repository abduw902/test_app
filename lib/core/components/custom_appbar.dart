import 'package:flutter/material.dart';
import 'package:test_app/core/components/custom_back_button.dart';
import 'package:test_app/core/components/style.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final AppBar appBar = AppBar();
  final bool implyBackButton;

  CustomAppBar({
    Key? key,
    this.title,
    this.implyBackButton = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: kBackgroundColor,
      centerTitle: true,
      title: Text(title ?? '', style: kBoldWhite24),
      iconTheme: const IconThemeData(
        color: kWhite,
      ),
      leading: implyBackButton ? const CustomBackButton() : null,
      elevation: 0,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(appBar.preferredSize.height);
}
