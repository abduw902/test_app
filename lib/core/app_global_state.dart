import 'package:hive_flutter/hive_flutter.dart';

class AppGlobalState {
  static final AppGlobalState _singleton = AppGlobalState._internal();

  factory AppGlobalState() => _singleton;

  static late Box hive;

  AppGlobalState._internal();

  static late bool isAppleSignInAvailable;

  Future<void> appSetUp() async {
    await Hive.initFlutter();
    hive = await Hive.openBox('globalHive');
  }
}
